﻿using System;
using System.IO;

namespace Day_01
{
    class Program
    {
        static void Main(string[] args)
        {

            var input = File.ReadAllLines("input.txt");
            var sum1 = 0;
            var sum2 = 0;

            foreach (var line in input)
            {
                var number = int.Parse(line);
                sum1 += Calculate(number);

                while (Calculate(number) > 0)
                {
                    sum2 += Calculate(number);
                    number = Calculate(number);
                }
            }

            Console.WriteLine("Part 1: " + sum1);
            Console.WriteLine("Part 2: " + sum2);
            Console.ReadKey();
        }
        public static int Calculate(int i) => i / 3 - 2;
    }
}
