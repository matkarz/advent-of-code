﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day_04
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines("input.txt");

            var output = 0;
            var input2 = new List<string>();
            foreach (var line in input)
            {
                var checksum = line.Split('[')[1].Replace("]", string.Empty);
                var number = int.Parse(line.Split('-').Last().Split('[').First());

                var chars = line.Remove(line.LastIndexOf('-')).Split('-').Aggregate((x, y) => x + y);

                string result = string.Empty;
                chars.GroupBy(o => o).OrderByDescending(o => o.Count()).ThenBy(o => o.Key).Take(5).ToList().ForEach(o => result += o.Key.ToString());
                if (result == checksum)
                {
                    output += number;
                    input2.Add(line.Split('[').First().Remove(line.LastIndexOf('-')) + "#" + number);
                }
            }
            Console.WriteLine(output);

            var newwords = new List<string>();
            foreach (var line in input2)
            {
                var number = int.Parse(line.Split("#").Last());
                var words = line.Split("#").First();

                var newline = "";
                foreach (var word in words.Split('-'))
                {
                    foreach (var c in word)
                    {
                        var cc = c + (number % 26);
                        newline += (char)(cc > 122 ? cc - 26 : cc);
                    }
                    newline += " ";
                }
                newwords.Add(newline.TrimEnd('-') + "#" + number.ToString());
            }
            Console.WriteLine(newwords.Where(o => o.StartsWith("northpole object")).First().Split('#').Last());
        }
    }
}